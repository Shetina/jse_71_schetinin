package ru.t1.schetinin.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.schetinin.tm.api.endpoint.IUserEndpoint;
import ru.t1.schetinin.tm.api.service.IAuthService;
import ru.t1.schetinin.tm.api.service.IServiceLocator;
import ru.t1.schetinin.tm.api.service.dto.IUserDTOService;
import ru.t1.schetinin.tm.dto.request.*;
import ru.t1.schetinin.tm.dto.response.*;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.dto.model.SessionDTO;
import ru.t1.schetinin.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.schetinin.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private IUserDTOService userDTOService;

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        try {
            userDTOService.lockUserByLogin(login);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        try {
            userDTOService.unlockUserByLogin(login);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        try {
            userDTOService.removeByLogin(login);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        check(request);
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        try {
            @NotNull final UserDTO user = authService.registry(login, password, email);
            return new UserRegistryResponse(user);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        try {
            userDTOService.setPassword(userId, password);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new UserChangePasswordResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        try {
            userDTOService.updateUser(userId, firstName, lastName, middleName);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new UserUpdateProfileResponse();
    }

}