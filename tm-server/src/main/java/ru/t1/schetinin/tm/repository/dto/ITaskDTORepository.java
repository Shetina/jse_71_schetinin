package ru.t1.schetinin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.dto.model.TaskDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    long countByUserId(@NotNull final String userId);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<TaskDTO> findByUserId(@NotNull final String userId);

    @NotNull
    List<TaskDTO> findByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<TaskDTO> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<TaskDTO> findByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
