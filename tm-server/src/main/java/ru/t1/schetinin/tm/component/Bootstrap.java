package ru.t1.schetinin.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.api.endpoint.*;
import ru.t1.schetinin.tm.api.service.*;
import ru.t1.schetinin.tm.api.service.dto.*;
import ru.t1.schetinin.tm.endpoint.*;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;
import ru.t1.schetinin.tm.service.*;
import ru.t1.schetinin.tm.service.dto.*;
import ru.t1.schetinin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @Getter
    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Getter
    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @Getter
    @NotNull
    @Autowired
    private IUserDTOService userService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @Getter
    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initEndpoints(@NotNull final AbstractEndpoint[] endpoints) {
        for (@NotNull AbstractEndpoint endpoint : endpoints) {
            registry(endpoint);
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void initDemoData() {
        try {
            getSessionService().clear(getUserService().findByLogin("admin").getId());
            getTaskService().clear(getUserService().findByLogin("admin").getId());
            getProjectService().clear(getUserService().findByLogin("admin").getId());
            getUserService().clear();
            getUserService().create("admin", "admin", Role.ADMIN);
            getUserService().create("test", "test", "test@test.ru");
            getProjectService().add(getUserService().findByLogin("admin").getId(), new ProjectDTO("TEST PROJECT", Status.IN_PROGRESS));
            getProjectService().add(getUserService().findByLogin("admin").getId(), new ProjectDTO("DEMO PROJECT", Status.IN_PROGRESS));
            getProjectService().add(getUserService().findByLogin("admin").getId(), new ProjectDTO("BETA PROJECT", Status.IN_PROGRESS));
            getTaskService().create(getUserService().findByLogin("admin").getId(), "MEGA TASK");
            getTaskService().create(getUserService().findByLogin("admin").getId(), "SUPER TASK");
            getTaskService().create(getUserService().findByLogin("admin").getId(), "CLEAN TASK");

        } catch (@NotNull final Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        initPID();
        initEndpoints(endpoints);
        //initDemoData();
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("*** TASK MANAGER IS SHOOTING DOWN ***");
    }

}