package ru.t1.schetinin.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.schetinin.tm.api.service.model.IService;
import ru.t1.schetinin.tm.enumerated.TMSort;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.model.AbstractModel;
import ru.t1.schetinin.tm.repository.model.IRepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected abstract IRepository<M> getRepository();

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) throws Exception {
        getRepository().save(model);
        return model;
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        getRepository().deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsById(id);
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final TMSort sort) throws Exception {
        if (sort == null) return findAll();
        @Nullable final Sort findSort;
        switch (sort.name()) {
            case "BY_NAME":
                findSort = Sort.by(Sort.Direction.ASC, "name");
                break;
            case "BY_STATUS":
                findSort = Sort.by(Sort.Direction.ASC, "status");
                break;
            default:
                findSort = Sort.by(Sort.Direction.ASC, "created");
        }
        return getRepository().findAll(findSort);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<M> result = getRepository().findById(id);
        return result.orElse(null);
    }

    @Override
    public int getSize() throws Exception {
        return (int) getRepository().count();
    }

    @Override
    @Transactional
    public void remove(@Nullable final M model) throws Exception {
        if (model == null) return;
        getRepository().delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        @Nullable M result = findOneById(id);
        remove(result);
    }

    @Override
    @Transactional
    public void update(@Nullable final M model) throws Exception {
        if (model == null) return;
        getRepository().save(model);
    }

}