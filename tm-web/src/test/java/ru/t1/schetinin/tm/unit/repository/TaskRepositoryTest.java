package ru.t1.schetinin.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.schetinin.tm.config.DataBaseConfiguration;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.dto.ProjectDTO;
import ru.t1.schetinin.tm.model.dto.TaskDTO;
import ru.t1.schetinin.tm.repository.ProjectDTORepository;
import ru.t1.schetinin.tm.repository.TaskDTORepository;
import ru.t1.schetinin.tm.util.UserUtil;

import java.util.List;
import java.util.Optional;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    public final static String USER_NAME = "admin2";

    @NotNull
    public final static String USER_PASSWORD = "admin2";

    @NotNull
    public final static String PROJECT1_NAME = "Project 1";

    @NotNull
    public final static String PROJECT1_DESCRIPTION = "Description 1";

    @NotNull
    private ProjectDTO project1 = new ProjectDTO(PROJECT1_NAME, PROJECT1_DESCRIPTION);

    @NotNull
    public final static String TASK1_NAME = "Task 1";

    @NotNull
    public final static String TASK1_DESCRIPTION = "Description 1";

    @NotNull
    public final static String TASK2_NAME = "Task 2";

    @NotNull
    public final static String TASK2_DESCRIPTION = "Description 2";

    @NotNull
    private TaskDTO task1 = new TaskDTO(TASK1_NAME, TASK1_DESCRIPTION);

    @NotNull
    private TaskDTO task2 = new TaskDTO(TASK2_NAME, TASK2_DESCRIPTION);

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private String userId;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_NAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(project1.getId());
        task2.setProjectId(project1.getId());
        projectRepository.save(project1);
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    @SneakyThrows
    public void after() {
        taskRepository.delete(task1);
        taskRepository.delete(task2);
        projectRepository.delete(project1);
    }

    @Test
    @SneakyThrows
    public void findByProjectIdTest() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findByProjectId(project1.getId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void countByUserIdTest() {
        Assert.assertEquals(2, taskRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdTest() {
        taskRepository.deleteByUserId(userId);
        Assert.assertEquals(0, taskRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdAndIdTest() {
        taskRepository.deleteByUserIdAndId(userId, task1.getId());
        Assert.assertEquals(1, taskRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void existByUserIdAndIdTest() {
        Assert.assertFalse(taskRepository.existsByUserIdAndId("", task1.getId()));
        Assert.assertFalse(taskRepository.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(taskRepository.existsByUserIdAndId(userId, task1.getId()));
    }

    @Test
    @SneakyThrows
    public void findByUserIdTest() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findByUserId(userId);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndIdTest() {
        @NotNull final Optional<TaskDTO> task = taskRepository.findByUserIdAndId(userId, task1.getId());
        Assert.assertEquals(task1.getId(), task.orElse(null).getId());
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndProjectIdTest() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findByUserIdAndProjectId(userId, project1.getId());
        Assert.assertEquals(2, tasks.size());
    }

}