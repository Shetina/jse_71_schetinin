package ru.t1.schetinin.tm.unit.repository;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.schetinin.tm.config.DataBaseConfiguration;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.dto.ProjectDTO;
import ru.t1.schetinin.tm.repository.ProjectDTORepository;
import ru.t1.schetinin.tm.util.UserUtil;

import java.util.List;
import java.util.Optional;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    public final static String USER_NAME = "admin2";

    @NotNull
    public final static String USER_PASSWORD = "admin2";

    @NotNull
    public final static String PROJECT1_NAME = "Project 1";

    @NotNull
    public final static String PROJECT1_DESCRIPTION = "Description 1";

    @NotNull
    public final static String PROJECT2_NAME = "Project 2";

    @NotNull
    public final static String PROJECT2_DESCRIPTION = "Description 2";

    @NotNull
    private ProjectDTO project1 = new ProjectDTO(PROJECT1_NAME, PROJECT1_DESCRIPTION);

    @NotNull
    private ProjectDTO project2 = new ProjectDTO(PROJECT2_NAME, PROJECT2_DESCRIPTION);

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private String userId;


    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_NAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        projectRepository.save(project1);
        projectRepository.save(project2);
    }

    @After
    @SneakyThrows
    public void after() {
        projectRepository.delete(project1);
        projectRepository.delete(project2);
    }

    @Test
    @SneakyThrows
    public void countByUserIdTest() {
        Assert.assertEquals(2, projectRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdTest() {
        projectRepository.deleteByUserId(userId);
        Assert.assertEquals(0, projectRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdAndIdTest() {
        projectRepository.deleteByUserIdAndId(userId, project1.getId());
        Assert.assertEquals(1, projectRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void existByUserIdAndIdTest() {
        Assert.assertFalse(projectRepository.existsByUserIdAndId("", project1.getId()));
        Assert.assertFalse(projectRepository.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(projectRepository.existsByUserIdAndId(userId, project1.getId()));
    }

    @Test
    @SneakyThrows
    public void findByUserIdTest() {
        @NotNull final List<ProjectDTO> projects = projectRepository.findByUserId(userId);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndIdTest() {
        @NotNull final Optional<ProjectDTO> project = projectRepository.findByUserIdAndId(userId, project1.getId());
        Assert.assertEquals(project1.getId(), project.orElse(null).getId());
    }

}