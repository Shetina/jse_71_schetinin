package ru.t1.schetinin.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.schetinin.tm.api.service.dto.IProjectDTOService;
import ru.t1.schetinin.tm.config.*;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.dto.ProjectDTO;
import ru.t1.schetinin.tm.model.dto.TaskDTO;
import ru.t1.schetinin.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, DataBaseConfiguration.class, SecurityWebApplicationInitializer.class, ServiceAuthenticationEntryPoint.class, WebApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @Nullable
    private String userId;

    @NotNull
    public final static String USER_NAME = "admin2";

    @NotNull
    public final static String USER_PASSWORD = "admin2";

    @NotNull
    public final static String PROJECT1_NAME = "Project 1";

    @NotNull
    public final static String PROJECT1_DESCRIPTION = "Description 1";

    @NotNull
    public final static String TASK1_NAME = "Task 1";

    @NotNull
    public final static String TASK1_DESCRIPTION = "Description 1";

    @NotNull
    public final static String TASK2_NAME = "Task 2";

    @NotNull
    public final static String TASK2_DESCRIPTION = "Description 2";

    @NotNull
    public final static String TASK3_NAME = "Task 3";

    @NotNull
    public final static String TASK3_DESCRIPTION = "Description 3";

    @NotNull
    private ProjectDTO project1 = new ProjectDTO(PROJECT1_NAME, PROJECT1_DESCRIPTION);

    @NotNull
    private TaskDTO task1 = new TaskDTO(TASK1_NAME, TASK1_DESCRIPTION);

    @NotNull
    private TaskDTO task2 = new TaskDTO(TASK2_NAME, TASK2_DESCRIPTION);

    @NotNull
    private TaskDTO task3 = new TaskDTO(TASK3_NAME, TASK3_DESCRIPTION);

    @NotNull
    private static final String TASKS_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/api/projects/";

    @Before
    public void before() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_NAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(project1.getId());
        task2.setProjectId(project1.getId());
        addProject(project1);
        add(task1);
        add(task2);
    }

    @After
    @SneakyThrows
    public void after() throws Exception {
        @NotNull final String url = TASKS_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @NotNull final String urlProjects = PROJECTS_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.post(urlProjects)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void addProject(@NotNull final ProjectDTO project) {
        @NotNull final String url = "http://localhost:8080/api/projects/add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void add(@NotNull final TaskDTO task) {
        @NotNull final String url = TASKS_URL + "add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    @SneakyThrows
    private TaskDTO findById(@NotNull final String id) {
        @NotNull final String url = TASKS_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDTO.class);
    }

    @Test
    public void findAllTest() throws Exception {
        @NotNull final String url = TASKS_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final List<TaskDTO> tasks = Arrays.asList(objectMapper.readValue(json, TaskDTO[].class));
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void findAllByProjectIdTest() throws Exception {
        @NotNull final String url = TASKS_URL + "findAllByProjectId/" + project1.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final List<TaskDTO> tasks = Arrays.asList(objectMapper.readValue(json, TaskDTO[].class));
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void addTest() throws Exception {
        add(task3);
        @Nullable final TaskDTO task = findById(task3.getId());
        Assert.assertNotNull(task);
    }

    @Test
    public void saveTest() throws Exception {
        task1.setStatus(Status.IN_PROGRESS);
        @NotNull final String url = TASKS_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task1);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final TaskDTO task = findById(task1.getId());
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void findByIdTest() throws Exception {
        Assert.assertNotNull(findById(task1.getId()));
    }

    @Test
    public void existsByIdTest() throws Exception {
        @NotNull final String url = TASKS_URL + "existsById/" + task1.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals("true", objectMapper.readValue(json, String.class));
    }

    @Test
    public void countTest() throws Exception {
        @NotNull final String url = TASKS_URL + "count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals(2, objectMapper.readValue(json, Integer.class).intValue());
    }

    @Test
    public void deleteByIdTest() throws Exception {
        @NotNull final String url = TASKS_URL + "deleteById/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(task1.getId()));
    }

    @Test
    public void deleteTest() throws Exception {
        @NotNull final String url = TASKS_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task1);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(task1.getId()));
    }

    @Test
    public void deleteAllTest() throws Exception {
        @NotNull final String url = TASKS_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(task1.getId()));
        Assert.assertNull(findById(task2.getId()));
    }

}