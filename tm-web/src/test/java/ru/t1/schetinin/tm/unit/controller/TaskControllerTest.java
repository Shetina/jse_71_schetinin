package ru.t1.schetinin.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.schetinin.tm.api.service.dto.ITaskDTOService;
import ru.t1.schetinin.tm.config.*;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.dto.ProjectDTO;
import ru.t1.schetinin.tm.model.dto.TaskDTO;
import ru.t1.schetinin.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, DataBaseConfiguration.class, SecurityWebApplicationInitializer.class, ServiceAuthenticationEntryPoint.class, WebApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskControllerTest {

    @NotNull
    public final static String USER_NAME = "admin2";

    @NotNull
    public final static String USER_PASSWORD = "admin2";

    @NotNull
    @Autowired

    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;


    @Nullable
    private static String userId;

    @NotNull
    public final static String TASK1_NAME = "Task 1";

    @NotNull
    public final static String TASK1_DESCRIPTION = "Description 1";

    @NotNull
    public final static String TASK2_NAME = "Task 2";

    @NotNull
    public final static String TASK2_DESCRIPTION = "Description 2";

    @NotNull
    public final static String TASK3_NAME = "Task 3";

    @NotNull
    public final static String TASK3_DESCRIPTION = "Description 3";

    @NotNull
    private TaskDTO task1 = new TaskDTO(TASK1_NAME, TASK1_DESCRIPTION);

    @NotNull
    private TaskDTO task2 = new TaskDTO(TASK2_NAME, TASK2_DESCRIPTION);

    @NotNull
    private TaskDTO task3 = new TaskDTO(TASK3_NAME, TASK3_DESCRIPTION);

    @Before
    public void before() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_NAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        taskService.add(task1);
        taskService.add(task2);
    }

    @After
    @SneakyThrows
    public void after() throws Exception {
        taskService.clear();
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull final String url = "/task/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final List<TaskDTO> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(3, tasks.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = "/task/delete/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final TaskDTO task = taskService.findOneById(task1.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final String url = "/task/edit/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}