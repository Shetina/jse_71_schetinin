package ru.t1.schetinin.tm.unit.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.schetinin.tm.api.service.dto.IProjectDTOService;
import ru.t1.schetinin.tm.api.service.dto.ITaskDTOService;
import ru.t1.schetinin.tm.config.DataBaseConfiguration;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.dto.ProjectDTO;
import ru.t1.schetinin.tm.model.dto.TaskDTO;
import ru.t1.schetinin.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    public final static String USER_NAME = "admin2";

    @NotNull
    public final static String USER_PASSWORD = "admin2";

    @NotNull
    public final static String PROJECT1_NAME = "Project 1";

    @NotNull
    public final static String PROJECT1_DESCRIPTION = "Description 1";

    @NotNull
    public final static String PROJECT2_NAME = "Project 2";

    @NotNull
    public final static String PROJECT2_DESCRIPTION = "Description 2";

    @NotNull
    public final static String TASK1_NAME = "Task 1";

    @NotNull
    public final static String TASK1_DESCRIPTION = "Description 1";

    @NotNull
    public final static String TASK2_NAME = "Task 2";

    @NotNull
    public final static String TASK2_DESCRIPTION = "Description 2";

    @NotNull
    public final static String TASK3_NAME = "Task 3";

    @NotNull
    public final static String TASK3_DESCRIPTION = "Description 3";

    @NotNull
    private ProjectDTO project1 = new ProjectDTO(PROJECT1_NAME, PROJECT1_DESCRIPTION);

    @NotNull
    private ProjectDTO project2 = new ProjectDTO(PROJECT2_NAME, PROJECT2_DESCRIPTION);

    @NotNull
    private TaskDTO task1 = new TaskDTO(TASK1_NAME, TASK1_DESCRIPTION);

    @NotNull
    private TaskDTO task2 = new TaskDTO(TASK2_NAME, TASK2_DESCRIPTION);

    @NotNull
    private TaskDTO task3 = new TaskDTO(TASK3_NAME, TASK3_DESCRIPTION);

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private String userId;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_NAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(project1.getId());
        task2.setProjectId(project1.getId());
        projectService.add(project1);
        projectService.add(project2);
        taskService.add(task1);
        taskService.add(task2);
    }

    @After
    @SneakyThrows
    public void after() {
        taskService.clear();
        projectService.clear();
    }

    @Test
    @SneakyThrows
    public void addTest() {
        taskService.add(task3);
        Assert.assertNotNull(taskService.findOneById(task3.getId()));
    }

    @Test
    @SneakyThrows
    public void addByUserIdTest() {
        taskService.addByUserId(userId, task3);
        Assert.assertNotNull(taskService.findOneByUserIdAndId(userId, task3.getId()));
    }

    @Test
    @SneakyThrows
    public void changeTaskStatusByIdTest() {
        taskService.changeTaskStatusById(task1.getId(), Status.IN_PROGRESS);
        @Nullable final TaskDTO task = taskService.findOneById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void changeTaskStatusByUserIdAndIdTest() {
        taskService.changeTaskStatusByUserIdAndId(userId, task1.getId(), Status.IN_PROGRESS);
        @Nullable final TaskDTO task = taskService.findOneByUserIdAndId(userId, task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        taskService.clear();
        Assert.assertEquals(0, taskService.count());
    }

    @Test
    @SneakyThrows
    public void clearByUserIdTest() {
        taskService.clearByUserId(userId);
        Assert.assertEquals(0, taskService.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void countTest() {
        Assert.assertEquals(2, taskService.count());
    }

    @Test
    @SneakyThrows
    public void countByUserIdTest() {
        Assert.assertEquals(2, taskService.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @Nullable final TaskDTO task = taskService.create(userId, TASK3_NAME);
        Assert.assertNotNull(taskService.findOneByUserIdAndId(userId, task.getId()));
    }

    @Test
    @SneakyThrows
    public void createWithDescriptionTest() {
        @Nullable final TaskDTO task = taskService.create(userId, TASK3_NAME, TASK3_DESCRIPTION);
        Assert.assertNotNull(taskService.findOneByUserIdAndId(userId, task.getId()));
    }

    @Test
    @SneakyThrows
    public void existsByIdTest() {
        Assert.assertFalse(taskService.existsById(""));
        Assert.assertTrue(taskService.existsById(task1.getId()));
    }

    @Test
    @SneakyThrows
    public void existsByUserIdAndIdTest() {
        Assert.assertFalse(taskService.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(taskService.existsByUserIdAndId(userId, task1.getId()));
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @Nullable final List<TaskDTO> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdTest() {
        @Nullable final List<TaskDTO> tasks = taskService.findAllByUserId(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findAllByProjectIdTest() {
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(project1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdAndProjectIdTest() {
        @Nullable final List<TaskDTO> tasks = taskService.findAllByUserIdAndProjectId(userId, project1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findOneByIdTest() {
        @Nullable final TaskDTO task = taskService.findOneById(task1.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @SneakyThrows
    public void findOneByUserIdAndIdTest() {
        @Nullable final TaskDTO task = taskService.findOneByUserIdAndId(userId, task1.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @SneakyThrows
    public void removeTest() {
        taskService.remove(task1);
        Assert.assertNull(taskService.findOneById(task1.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByUserIdTest() {
        taskService.removeByUserId(userId, task1);
        Assert.assertNull(taskService.findOneByUserIdAndId(userId, task1.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByIdTest() {
        taskService.removeById(task1.getId());
        Assert.assertNull(taskService.findOneById(task1.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByUserIdAndIdTest() {
        taskService.removeByUserIdAndId(userId, task1.getId());
        Assert.assertNull(taskService.findOneByUserIdAndId(userId, task1.getId()));
    }

    @Test
    @SneakyThrows
    public void updateTest() {
        task1.setStatus(Status.IN_PROGRESS);
        @Nullable final TaskDTO task = taskService.update(task1);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void updateByUserIdTest() {
        task1.setStatus(Status.IN_PROGRESS);
        @Nullable final TaskDTO task = taskService.updateByUserId(userId, task1);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void updateByIdTest() {
        @Nullable final TaskDTO task = taskService.updateById(task1.getId(), TASK3_NAME, TASK3_DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), TASK3_NAME);
        Assert.assertEquals(task.getDescription(), TASK3_DESCRIPTION);
    }

    @Test
    @SneakyThrows
    public void updateByUserIdAndIdTest() {
        @Nullable final TaskDTO task = taskService.updateByUserIdAndId(userId, task1.getId(), TASK3_NAME, TASK3_DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), TASK3_NAME);
        Assert.assertEquals(task.getDescription(), TASK3_DESCRIPTION);
    }

    @Test
    @SneakyThrows
    public void updateProjectIdByIdTest() {
        taskService.updateProjectIdById(task1.getId(), project1.getId());
        Assert.assertNotNull(taskService.findOneById(task1.getId()));
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(project1.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    @SneakyThrows
    public void updateProjectIdByUserIdAndIdTest() {
        taskService.updateProjectIdByUserIdAndId(userId, task1.getId(), project1.getId());
        Assert.assertNotNull(taskService.findOneByUserIdAndId(userId, task1.getId()));
        @Nullable final List<TaskDTO> tasks = taskService.findAllByUserIdAndProjectId(userId, project1.getId());
        Assert.assertNotNull(tasks);
    }

}