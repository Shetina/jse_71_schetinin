package ru.t1.schetinin.tm.unit.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.schetinin.tm.api.service.dto.IProjectDTOService;
import ru.t1.schetinin.tm.config.*;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Result;
import ru.t1.schetinin.tm.model.dto.ProjectDTO;
import ru.t1.schetinin.tm.model.dto.UserDTO;
import ru.t1.schetinin.tm.util.UserUtil;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, DataBaseConfiguration.class, SecurityWebApplicationInitializer.class, ServiceAuthenticationEntryPoint.class, WebApplicationConfiguration.class})
@Category(UnitCategory.class)
public class ProjectEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @Nullable
    private String userId;

    @NotNull
    public final static String USER_NAME = "admin2";

    @NotNull
    public final static String USER_PASSWORD = "admin2";

    @NotNull
    public final static String PROJECT1_NAME = "Project 1";

    @NotNull
    public final static String PROJECT1_DESCRIPTION = "Description 1";

    @NotNull
    public final static String PROJECT2_NAME = "Project 2";

    @NotNull
    public final static String PROJECT2_DESCRIPTION = "Description 2";

    @NotNull
    public final static String PROJECT3_NAME = "Project 3";

    @NotNull
    public final static String PROJECT3_DESCRIPTION = "Description 3";

    @NotNull
    private ProjectDTO project1 = new ProjectDTO(PROJECT1_NAME, PROJECT1_DESCRIPTION);

    @NotNull
    private ProjectDTO project2 = new ProjectDTO(PROJECT2_NAME, PROJECT2_DESCRIPTION);

    @NotNull
    private ProjectDTO project3 = new ProjectDTO(PROJECT3_NAME, PROJECT3_DESCRIPTION);

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/projects/";

    @Before
    public void before() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_NAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        add(project1);
        add(project2);
    }

    @After
    @SneakyThrows
    public void after() throws Exception {
        @NotNull final String url = BASE_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void add(@NotNull final ProjectDTO project) {
        @NotNull final String url = BASE_URL + "add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    @SneakyThrows
    private ProjectDTO findById(@NotNull final String id) {
        @NotNull final String url = BASE_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, ProjectDTO.class);
    }

    @Test
    public void findAllTest() throws Exception {
        @NotNull final String url = BASE_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final List<ProjectDTO> projects = Arrays.asList(objectMapper.readValue(json, ProjectDTO[].class));
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void addTest() throws Exception {
        add(project3);
        @Nullable final ProjectDTO project = findById(project3.getId());
        Assert.assertNotNull(project);
    }

    @Test
    public void saveTest() throws Exception {
        project1.setStatus(Status.IN_PROGRESS);
        @NotNull final String url = BASE_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project1);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final ProjectDTO project = findById(project1.getId());
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void findByIdTest() throws Exception {
        Assert.assertNotNull(findById(project1.getId()));
    }

    @Test
    public void existsByIdTest() throws Exception {
        @NotNull final String url = BASE_URL + "existsById/" + project1.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals("true", objectMapper.readValue(json, String.class));
    }

    @Test
    public void countTest() throws Exception {
        @NotNull final String url = BASE_URL + "count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals(2, objectMapper.readValue(json, Integer.class).intValue());
    }

    @Test
    public void deleteByIdTest() throws Exception {
        @NotNull final String url = BASE_URL + "deleteById/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
    }

    @Test
    public void deleteTest() throws Exception {
        @NotNull final String url = BASE_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project1);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
    }

    @Test
    public void deleteAllTest() throws Exception {
        @NotNull final String url = BASE_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
        Assert.assertNull(findById(project2.getId()));
    }

}