package ru.t1.schetinin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.schetinin.tm.model.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface TaskEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<TaskDTO> findAll() throws Exception;

    @WebMethod
    @GetMapping("/findAllByProjectId/{projectId}")
    List<TaskDTO> findAllByProjectId(@WebParam(name = "projectId", partName = "projectId") @PathVariable("projectId") String projectId) throws Exception;

    @WebMethod
    @PostMapping("/add")
    TaskDTO add(@WebParam(name = "task", partName = "task") @RequestBody TaskDTO task) throws Exception;

    @WebMethod
    @PostMapping("/save")
    TaskDTO save(@WebParam(name = "task", partName = "task") @RequestBody TaskDTO task) throws Exception;

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDTO findById(@WebParam(name = "id", partName = "id") @PathVariable("id") String id) throws Exception;

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(@WebParam(name = "id", partName = "id") @PathVariable("id") String id) throws Exception;

    @WebMethod
    @GetMapping("/count")
    long count() throws Exception;

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(@WebParam(name = "id", partName = "id") @PathVariable("id") String id) throws Exception;

    @WebMethod
    @PostMapping("/delete")
    void delete(@WebParam(name = "task", partName = "task") @RequestBody TaskDTO task) throws Exception;

    @WebMethod
    @PostMapping("/deleteAll")
    void clear() throws Exception;

}
