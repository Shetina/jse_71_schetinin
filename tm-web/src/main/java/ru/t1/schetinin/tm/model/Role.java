package ru.t1.schetinin.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "role_table")
public final class Role {

    @Id
    @Column
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @Column(name = "role_type")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}
