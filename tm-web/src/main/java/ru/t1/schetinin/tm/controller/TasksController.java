package ru.t1.schetinin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.schetinin.tm.api.service.model.IProjectService;
import ru.t1.schetinin.tm.api.service.model.ITaskService;
import ru.t1.schetinin.tm.model.dto.CustomUser;
import ru.t1.schetinin.tm.repository.ProjectDTORepository;
import ru.t1.schetinin.tm.repository.TaskDTORepository;

@Controller
public class TasksController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/tasks")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAllByUserId(user.getUserId()));
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}