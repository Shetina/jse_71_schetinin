package ru.t1.schetinin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.schetinin.tm.model.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/users")
public interface UserEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<UserDTO> findAll() throws Exception;

    @WebMethod
    @PostMapping("/add")
    UserDTO add(@WebParam(name = "user", partName = "user") @RequestBody UserDTO task) throws Exception;

    @WebMethod
    @PostMapping("/save")
    UserDTO save(@WebParam(name = "user", partName = "user") @RequestBody UserDTO user) throws Exception;

    @WebMethod
    @GetMapping("/findById/{id}")
    UserDTO findById(@WebParam(name = "id", partName = "id") @PathVariable("id") String id) throws Exception;

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(@WebParam(name = "id", partName = "id") @PathVariable("id") String id) throws Exception;

    @WebMethod
    @GetMapping("/count")
    long count() throws Exception;

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(@WebParam(name = "id", partName = "id") @PathVariable("id") String id) throws Exception;

    @WebMethod
    @PostMapping("/delete")
    void delete(@WebParam(name = "user", partName = "user") @RequestBody UserDTO user) throws Exception;

    @WebMethod
    @PostMapping("/deleteAll")
    void clear() throws Exception;

}