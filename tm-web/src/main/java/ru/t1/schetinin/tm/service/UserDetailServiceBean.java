package ru.t1.schetinin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.schetinin.tm.enumerated.RoleType;
import ru.t1.schetinin.tm.model.Role;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.model.dto.CustomUser;
import ru.t1.schetinin.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("userDetailService")
public class UserDetailServiceBean implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {
        @Nullable final User user = userRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);

        @NotNull final List<Role> userRoles = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        for (final Role role : userRoles) roles.add(role.toString());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    private void init() {
        initUser("admin2", "admin2", RoleType.ADMINISTRATOR);
        initUser("test2", "test2", RoleType.USER);
    }

    public User findByLogin(@NotNull final String login) {
        return userRepository.findByLogin(login);
    }

    private void initUser(@NotNull final String login, @NotNull final String password, @NotNull final RoleType roleType) {
        @NotNull final User user = findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Transactional
    private void createUser(@Nullable final String login, @Nullable final String password, @NotNull final RoleType roleType) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        @NotNull final Role role = new Role();
        role.setRoleType(roleType);
        role.setUser(user);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

}
